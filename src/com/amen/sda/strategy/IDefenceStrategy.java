package com.amen.sda.strategy;

public interface IDefenceStrategy {
    void defendYourself();
}
