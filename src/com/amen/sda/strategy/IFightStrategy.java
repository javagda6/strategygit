package com.amen.sda.strategy;

public interface IFightStrategy {
    void fight(); // metoda walcz() z rysunku
//    void runaway(); // jesli dopiszemy metodę w interfejsie
                    // to we wszystkich klasach które implementują
                    // ten interfejs musimy stworzyć implementację
                    // dla tej metody (musimy je nadpisać)
}
