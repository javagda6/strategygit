package com.amen.sda.strategy.graphics;

public class Main {
    public static void main(String[] args) {
        GraphicsCard card = new GraphicsCard();

        card.setSetting(new HDSetting());
        card.printHowMuchPowerIsNeeded();

        card.setSetting(new LowSetting());
        card.printHowMuchPowerIsNeeded();
    }
}
