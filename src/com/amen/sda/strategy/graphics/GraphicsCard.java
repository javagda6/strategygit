package com.amen.sda.strategy.graphics;

public class GraphicsCard {
    private IGraphicsSetting setting;

    public IGraphicsSetting getSetting() {
        return setting;
    }

    public void setSetting(IGraphicsSetting setting) {
        this.setting = setting;
    }

    public void printHowMuchPowerIsNeeded() {
        System.out.println("I need power: " + setting.getProcessingPower());
    }
}
