package com.amen.sda.strategy.graphics;

public class HDSetting implements IGraphicsSetting {
    @Override
    public int getProcessingPower() {
        return 1000;
    }
}
