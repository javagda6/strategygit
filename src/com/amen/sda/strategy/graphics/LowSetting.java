package com.amen.sda.strategy.graphics;

public class LowSetting implements IGraphicsSetting {

    @Override
    public int getProcessingPower() {
        return 100;
    }
}
