package com.amen.sda.strategy.graphics;

public interface IGraphicsSetting {
    int getProcessingPower();
}
