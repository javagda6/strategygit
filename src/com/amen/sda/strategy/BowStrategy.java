package com.amen.sda.strategy;

public class BowStrategy implements IFightStrategy {
    @Override
    public void fight() {
        System.out.println("Walczę łukiem");
    }
}
