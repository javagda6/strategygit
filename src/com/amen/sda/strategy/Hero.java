package com.amen.sda.strategy;

public class Hero {
    private IFightStrategy fightStrategy;

    public IFightStrategy getFightStrategy() {
        return fightStrategy;
    }

    public void setFightStrategy(IFightStrategy fightStrategy) {
        this.fightStrategy = fightStrategy;
    }

    public void fightUsingStrategy(){
        fightStrategy.fight();
    }
}
