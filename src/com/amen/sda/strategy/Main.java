package com.amen.sda.strategy;

public class Main {

    public static void main(String[] args) {
        Hero h = new Hero();
        h.setFightStrategy(new SwordStrategy());
        h.fightUsingStrategy(); // wlacze mieczem

        h.setFightStrategy(new BowStrategy());
        h.fightUsingStrategy(); // walcze lukiem

        h.setFightStrategy(new SheepStrategy());
        h.fightUsingStrategy();
    }
}
